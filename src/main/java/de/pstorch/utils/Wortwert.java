package de.pstorch.utils;

public class Wortwert {

    private static final String CHAR_REFERENCE = "abcdefghijklmnopqrstuvwxyz";

    public int[] encodeCharsWithInts(final String input) {
        final String lowerString = input.toLowerCase();
        final int[] result = new int[lowerString.length()];
        for (int i = 0; i < lowerString.length(); i++) {
            final char c = lowerString.charAt(i);
            final int pos = CHAR_REFERENCE.indexOf(c) + 1;
            if (pos == -1) {
                throw new IllegalArgumentException("Char " + c + " can't be encoded");
            }
            result[i] = pos;
        }
        return result;
    }

    public String encodeIntsWithChars(final int[] ints) {
        final StringBuilder result = new StringBuilder();
        for (final int index : ints) {
            if (index < 1 || index > 26) {
                result.append(" ");
            } else {
                result.append(CHAR_REFERENCE.charAt(index - 1));
            }
        }
        return result.toString();
    }

    public int sum(final String input) {
        return sum(encodeCharsWithInts(input));
    }

    public int sum(final int[] ints) {
        int sum = 0;
        for (final int anInt : ints) {
            sum += anInt;
        }
        return sum;
    }

}
