package de.pstorch.utils;

import java.math.BigDecimal;

public class TerabyteInBits {

    private static final BigDecimal VALUE_OF_1024 = BigDecimal.valueOf(1024);

    public static void main(final String[] args) throws Exception {
        System.out.println(VALUE_OF_1024.multiply(VALUE_OF_1024)
                .multiply(VALUE_OF_1024).multiply(VALUE_OF_1024)
                .multiply(BigDecimal.valueOf(8)).toPlainString());
    }

}
