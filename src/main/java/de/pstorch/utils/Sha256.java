package de.pstorch.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Sha256 {

    private static final String expected = "0d119c00e2c65fe8021de9e97652b6e74ae29118b1e69c132bf2c6866964187b";

    public static void main(final String[] args) throws Exception {
        final long start = System.currentTimeMillis();
        final boolean found = findDigest();
        final long stop = System.currentTimeMillis();
        if (found) {
            System.out.println("Found :)");
        } else {
            System.out.println("Not Found :(");
        }
        System.out.println("Runtime: " + (stop - start) / 1000 + " sec.");
    }

    private static boolean findDigest() throws NoSuchAlgorithmException {
        final MessageDigest md = MessageDigest.getInstance("SHA-256");
        final NumberFormat df = DecimalFormat.getInstance();
        df.setMinimumIntegerDigits(3);

        for (int nord = 12000; nord < 14000; nord++) {
            for (int ost = 39000; ost < 41000; ost++) {
                md.reset();
                final String input = "Nord 50� " + nord / 1000 + "."
                        + df.format(nord % 1000) + " Ost 008� " + ost / 1000
                        + "." + df.format(ost % 1000);
                System.out.println(input);
                md.update(input.getBytes());

                final byte[] mdbytes = md.digest();

                // convert the byte to hex format method 1
                final StringBuilder sb = new StringBuilder();
                for (final byte mdbyte : mdbytes) {
                    sb.append(Integer.toString((mdbyte & 0xff) + 0x100, 16)
                            .substring(1));
                }

                final String hex = sb.toString();
                if (hex.equals(expected)) {
                    System.out.println("Found!");
                    return true;
                }
            }
        }
        return false;
    }

}
