package de.pstorch.utils;

import java.math.BigInteger;

public class Primezahlen {

    private static final long primes[] = {
            13,
            5927,
            439883
    };

    static boolean isPrime(final int n) {
        int counter = 2;
        final int n_halbe = n / 2;
        boolean value = true;
        while ((counter < n_halbe) && (value)) {
            if ((n % counter) == 0)
                value = false;
            counter++;
        }
        return value;
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {
        int index = 0;
        int prime = 0;
        int primeCounter = 0;
        while (index < primes.length) {
            if (BigInteger.valueOf(prime).isProbablePrime(100)) { //.isPrime(prime)) {
                primeCounter++;
                if (prime == primes[index]) {
                    System.out.println(primeCounter + ":" + prime);
                    index++;
                }
            }
            prime++;
        }

    }

}
