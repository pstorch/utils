package de.pstorch.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CoordinateConverterTest {

    @Test
    public void testParseCoordinate() {
        Assertions.assertEquals(50.559817, CoordinateConverter.parseCoordinate("N 50° 33.589", true), 0.000001);
        Assertions.assertEquals(9.762717, CoordinateConverter.parseCoordinate("E 09° 45.763", false), 0.000001);
    }

}
