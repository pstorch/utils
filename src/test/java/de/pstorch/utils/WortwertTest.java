package de.pstorch.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class WortwertTest {

    private final Wortwert wortwert = new Wortwert();

    @Test
    public void testEncodeCharsWithInts() {
        Assertions.assertTrue(Arrays.equals(wortwert.encodeCharsWithInts("WortWert"), new int[]{23, 15, 18, 20, 23, 5, 18, 20}));
    }

    @Test
    public void testEncodeIntsWithChars() {
        Assertions.assertEquals(wortwert.encodeIntsWithChars(new int[]{23, 15, 18, 20, 23, 5, 18, 20}), "wortwert");
    }

    @Test
    public void testSumIntArray() {
        Assertions.assertEquals(wortwert.sum(new int[]{20, 25, 33}), 78);
    }

    @Test
    public void testSumString() {
        Assertions.assertEquals(wortwert.sum("WortWert"), 142);
    }

}
